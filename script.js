
const books = [
  { 
    author: "Скотт Бэккер",
    name: "Тьма, что приходит прежде",
    price: 70 
  }, 
  {
   author: "Скотт Бэккер",
   name: "Воин-пророк",
  }, 
  { 
    name: "Тысячекратная мысль",
    price: 70
  }, 
  { 
    author: "Скотт Бэккер",
    name: "Нечестивый Консульт",
    price: 70
  }, 
  {
   author: "Дарья Донцова",
   name: "Детектив на диете",
   price: 40
  },
  {
   author: "Дарья Донцова",
   name: "Дед Снегур и Морозочка",
  }
];

const div = document.querySelector('#root');
const ul = document.createElement('ul');
div.appendChild(ul);


for( let key in books) {
  let keys = books[key];
  let validProperty = keys.author && keys.name && keys.price;
  try {
    if(! keys.author ) {
      throw new SyntaxError("author is not valid!");
    }
    if(! keys.name ) {
      throw new SyntaxError("name is not valid!");
    }
    if(! keys.price ) {
      throw new SyntaxError("price is not valid!");
    }
    const li = document.createElement('li');
    ul.appendChild(li);
    li.innerHTML = keys.author + ", " +  keys.name +  ", " + keys.price;
  } catch (error) {
    console.log(error.message);
  }
  
}




